# ShadowNode Wiki [![Documentation Status](https://readthedocs.org/projects/shadownode-wiki/badge/?version=latest)](https://shadownode-wiki.readthedocs.io/en/latest/?badge=latest)


This wiki contains the documentation for the ShadowNode community. Please suggest changes and additions as you see fit.

## Local Environment
If you'd like to make changes and/or additions please set up your local environment correctly so you can see the changes instantly.

1. [Python 3](https://www.python.org/)
2. [node.js](https://nodejs.org/)
3. [yarn](https://yarnpkg.com/)

Open bash, terminal, or other relevant command link in the root directory and run the following commands:
	
	yarn install
	pip install -r requirements.txt
	yarn run gulp

You can view your local changes in the new browser that was opened. When a change is make to the documentation, your browser will auto refresh itself to reflect the aforementioned change.

## Deployment
When you are ready to make your pull request to the main Github (this one), please run the follow command

	sphinx-build -b html source public
